import json

def creer_balise():
    id = int(input("Id: "))
    titre = input("Titre : ")
    description = input("Description : ")
    heure = ""
    nom_background = "soir"
    nom_image = "DallE/" + input("Nom de l'image : ")
    checkpoint = "true"
    
    reponses = []
    texte = "Suivant"
    min_alcool = 0
    max_alcool = 0
    objets_requis = []
    chapitre_destination = id+1
    augmente_alcool = "false"
    
    reponse = {
        "texte": texte,
        "min_alcool": min_alcool,
        "max_alcool": max_alcool,
        "objets_requis": objets_requis,
        "chapitre_destination": chapitre_destination,
        "augmente_alcool": augmente_alcool
    }
    reponses.append(reponse)

    balise = {
        "id": id,
        "titre": titre,
        "description": description,
        "heure": heure,
        "nom_background": nom_background,
        "nom_image": nom_image,
        "checkpoint": checkpoint,
        "reponses": reponses
    }
    return balise

# Exemple d'utilisation
balise_exemple = creer_balise()

# Conversion en format JSON
balise_json = json.dumps(balise_exemple, indent=2)
print(balise_json)